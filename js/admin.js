jQuery(document).ready(function(){
    var locationLat = jQuery('#events_metabox_location_lat'),
        locationLng = jQuery('#events_metabox_location_lng');

    if(locationLat.length && locationLng.length) {
        mapInit(locationLat.val(), locationLng.val());
    }


    var dateFormat = "mm/dd/yy",
        from = jQuery( "#events_metabox_date_from" )
            .datepicker({
                defaultDate: "+w",
                changeMonth: true,
                numberOfMonths: 1,
                altField: "#events_metabox_date_start",
                altFormat: "yy-mm-dd",
                minDate: new Date()
            })
            .on( "change", function() {
                to.datepicker( "option", "minDate", this.value );
            }),
        to = jQuery( "#events_metabox_date_to" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            altField: "#events_metabox_date_end",
            altFormat: "yy-mm-dd",
            minDate: new Date()
        })
        .on( "change", function() {
            from.datepicker( "option", "maxDate", this.value );
        });



});

function getDate( element ) {
    var date;
    try {
        date = $.datepicker.parseDate( dateFormat, element.value );
    } catch( error ) {
        date = null;
    }

    return date;
}

var  autocomplete;

function initGooleMapApi() {
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
        {types: ['geocode']});

    autocomplete.addListener('place_changed', fillInAddress);

    var inputLat = document.getElementById('events_metabox_location_lat').value;
    var inputLng = document.getElementById('events_metabox_location_lng').value;

    mapInit(inputLat, inputLng);
}

function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace(),
        lat = place.geometry.location.lat(),
        lng = place.geometry.location.lng(),
        inputLat = document.getElementById('events_metabox_location_lat'),
        inputLng = document.getElementById('events_metabox_location_lng');

    inputLat.value = lat;
    inputLng.value = lng;
    mapInit(lat, lng)
}

function mapInit(lat, lng) {
    if(lat === undefined) lat = -25.363;
    if(lng === undefined) lng = 131.044;

    var uluru = {lat: +lat, lng: +lng};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: uluru
    });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map
    });
}