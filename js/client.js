jQuery(document).ready(function(){

    //javasctipt
    //load more click function: improved to prevent double click and fire funciton only after content has been loaded (good for slow internet connection)
    jQuery('.load_more:not(.loading)').live('click',function(e){
        e.preventDefault();
        var $load_more_btn = jQuery(this);
        var post_type = headJS.cptPostType; // this is optional and can be set from anywhere, stored in mockup etc...
        var offset = calculatePostOffset(); //jQuery('.cpt-event-list .event-item').length;
        var nonce = $load_more_btn.attr('data-nonce');
        jQuery.ajax({
            type : "post",
            context: this,
            dataType : "json",
            url : headJS.ajaxurl,
            data : {
                action: "load_more",
                offset: offset,
                nonce: nonce,
                post_type: post_type,
                only_future: jQuery(this).data('only-future'),
                category:jQuery(this).data('category'),
                posts_per_page:jQuery(this).data('posts_per_page')
            },
            beforeSend: function(data) {
                // here u can do some loading animation...
                $load_more_btn.addClass('loading').html('Loading...');// good for styling and also to prevent ajax calls before content is loaded by adding loading class
            },
            success: function(response) {

                if (response['have_posts'] == 1){//if have posts:
                    $load_more_btn.removeClass('loading').html('Load More');
                    var $newElems = jQuery(response['html'].replace(/(\r\n|\n|\r)/gm, ''));// here removing extra breaklines and spaces
                    jQuery('.cpt-event-list').append($newElems);
                } else {
                    //end of posts (no posts found)
                    $load_more_btn.removeClass('loading').addClass('end_of_posts').html('<span>End of posts</span>'); // change buttom styles if no more posts
                }
            }
        });
    });

});

function calculatePostOffset() {
    return document.getElementsByClassName('event-item').length;
}

function initGooleMap() {
    var mapWrapper = document.getElementById('cpt-event-map');
    var uluru = {
        lat: +mapWrapper.dataset.lat,
        lng: +mapWrapper.dataset.lng
    };
    console.log(uluru);
    var map = new google.maps.Map(mapWrapper, {
        zoom: 13,
        center: uluru
    });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map
    });
}
