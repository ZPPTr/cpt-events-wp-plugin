<?php
get_header();

$event_meta = new CPT_Event_Meta_Data(get_the_ID());
$address = $event_meta->location_name ? '<small>'.$event_meta->location_name.'</small>' : '';
$date_format = get_option( 'date_format' );

?>
<?php if(have_posts() ) :
	the_post(); ?>
	<div <?php post_class('event-item single-event') ?>>
		<?php if(has_post_thumbnail()) : ?>
			<div class="event-media">
				<?php the_post_thumbnail('post-thumbnail') ?>
			</div>
		<?php endif; ?>
		<div class="event-content">
			<header>
				<?php the_title('<h1>', $address.'</h1>'); ?>
				<div class="event-date">
					<?php if($event_meta->date_start) :?>
						<span class="date-start">
					<strong><?php esc_html_e('Event Start Date:', 'cpt-events') ?></strong>
					<time datetime="<?php echo esc_attr(date($date_format, strtotime($event_meta->date_start))) ?>"><?php echo esc_attr(date($date_format, strtotime($event_meta->date_start))) ?></time>
				</span>
					<?php endif; ?>
					<?php if($event_meta->date_end) :?>
						<span class="date-end">
					<strong><?php esc_html_e('Event End Date:', 'cpt-events') ?></strong>
					<time datetime="<?php echo esc_attr(date($date_format), strtotime($event_meta->date_end)) ?>"><?php echo esc_attr(date($date_format, strtotime($event_meta->date_end))) ?></time>
				</span>
					<?php endif; ?>
				</div>
			</header>
			<?php the_excerpt() ?>
		</div>
		<?php if($event_meta->location_lat && $event_meta->location_lng) : ?>
			<div id="cpt-event-map" data-lat="<?php echo esc_attr($event_meta->location_lat) ?>" data-lng="<?php echo esc_attr($event_meta->location_lng) ?>"></div>
			<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo esc_attr(CPT_Events::GOOGLE_MAPS_API_KEY) ?>&libraries=places&callback=initGooleMap" async defer></script>
		<?php endif; ?>
	</div>

<?php endif ?>
<?php get_footer() ?>


