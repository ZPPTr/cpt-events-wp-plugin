<?php
/***
 * @var $item_meta object
 * @var $date_format string
 */

$address = $item_meta->location_name ? '<small>'.$item_meta->location_name.'</small>' : '';
?>
<div <?php post_class('event-item') ?>>
	<?php if(has_post_thumbnail()) : ?>
	<div class="event-media">
		<?php the_post_thumbnail('cpt-event-icon') ?>
	</div>
	<?php endif; ?>
	<div class="event-content">
		<header>
			<?php the_title('<h3><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">', '</a>'.$address.'</h3>'); ?>
			<div class="event-date">
				<?php if($item_meta->date_start) :?>
				<span class="date-start">
					<strong><?php esc_html_e('Event Start Date:', 'cpt-events') ?></strong>
					<time datetime="<?php echo esc_attr(date($date_format, $item_meta->date_start)) ?>">
						<?php echo esc_html(date($date_format, strtotime($item_meta->date_start))) ?>
					</time>
				</span>
				<?php endif; ?>
				<?php if($item_meta->date_end) :?>
					<span class="date-end">
					<strong><?php esc_html_e('Event End Date:', 'cpt-events') ?></strong>
					<time datetime="<?php echo esc_attr(date($date_format), $item_meta->date_end) ?>">
						<?php echo esc_html(date($date_format, strtotime($item_meta->date_end))) ?>
					</time>
				</span>
				<?php endif; ?>
			</div>
		</header>
		<?php the_excerpt() ?>
	</div>
</div>



