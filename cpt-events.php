<?php
/*
Plugin Name: Custom Post Type Events
Description: Testing Task For SintezTechnologies
Version:     1.0
Author:      Andrey Vodolagin
Author URI:  https://bitbucket.org/ZPPTr/
License:
*/

define('CPT_PLUGIN_VERSION', '1.0');
define('CPT_PLUGIN_PATH', plugin_dir_path(__FILE__));

$events = new CPT_Events();
$events->init();

class CPT_Events
{

	const POST_TYPE = 'cpt-events';
	const TAXONOMY_NAME = 'cpt-events-category';
	const GOOGLE_MAPS_API_KEY = 'AIzaSyAR8o987obNzPvNW4QGBDK14AClCfUC-2Q';

	private $slug = 'event';
	private $taxonomy_slug = 'events';

	public function init() {

		add_action( 'init', array( $this, 'cpt_action_register_post_type' ) );
		add_action( 'init', array( $this, 'cpt_action_register_taxonomy' ) );

		add_image_size('cpt-event-icon', 300, 300, true);

		if ( is_admin() ) {
			$this->add_admin_actions();
			$this->add_admin_filters();
		} else {
			$this->add_client_actions();
			$this->add_client_filters();
		}

	}


	public function cpt_action_register_post_type()
	{
		$post_names = apply_filters( 'cpt_events_post_type_name',
			array(
				'singular' => esc_html__( 'Event', 'cpt-events' ),
				'plural'   => esc_html__( 'Events', 'cpt-events' ),
			) );

		register_post_type( self::POST_TYPE,
			array(
				'labels'             => array(
					'name'               => $post_names['plural'], //__( 'Services', 'cpt-events' ),
					'singular_name'      => $post_names['singular'], //__( 'Services service', 'cpt-events' ),
					'add_new'            =>esc_html__( 'Add New', 'cpt-events' ),
					'add_new_item'       => sprintf(esc_html__( 'Add New %s', 'cpt-events' ), $post_names['singular'] ),
					'edit'               =>esc_html__( 'Edit', 'cpt-events' ),
					'edit_item'          => sprintf(esc_html__( 'Edit %s', 'cpt-events' ), $post_names['singular'] ),
					'new_item'           => sprintf(esc_html__( 'New %s', 'cpt-events' ), $post_names['singular'] ),
					'all_items'          => sprintf(esc_html__( 'All %s', 'cpt-events' ), $post_names['plural'] ),
					'view'               => sprintf(esc_html__( 'View %s', 'cpt-events' ), $post_names['singular'] ),
					'view_item'          => sprintf(esc_html__( 'View %s', 'cpt-events' ), $post_names['singular'] ),
					'search_items'       => sprintf(esc_html__( 'Search %s', 'cpt-events' ), $post_names['plural'] ),
					'not_found'          => sprintf(esc_html__( 'No %s Found', 'cpt-events' ), $post_names['plural'] ),
					'not_found_in_trash' => sprintf(esc_html__( 'No %s Found In Trash', 'cpt-events' ), $post_names['plural'] ),
					'parent_item_colon'  => '' /* text for parent types */
				),
				'description'        => sprintf(esc_html__( 'Create a %s item', 'cpt-events' ), $post_names['plural'] ),
				'public'             => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'publicly_queryable' => true,
				/* queries can be performed on the front end */
				'has_archive'        => true,
				'rewrite'            => array(
					'slug' => $this->slug
				),
				'menu_position'      => 4,
				'show_in_nav_menus'  => true,
				'menu_icon'          => 'dashicons-hammer',
				'hierarchical'       => false,
				'query_var'          => true,
				/* Sets the query_var key for this post type. Default: true - set to $post_type */
				'supports'           => array(
					'title', /* Text input field to create a post title. */
					'editor',
					'thumbnail', /* Displays a box for featured image. */
					'excerpt',
				),
				'capabilities'       => array(
					'edit_post'              => 'edit_pages',
					'read_post'              => 'edit_pages',
					'delete_post'            => 'edit_pages',
					'edit_posts'             => 'edit_pages',
					'edit_others_posts'      => 'edit_pages',
					'publish_posts'          => 'edit_pages',
					'read_private_posts'     => 'edit_pages',
					'read'                   => 'edit_pages',
					'delete_posts'           => 'edit_pages',
					'delete_private_posts'   => 'edit_pages',
					'delete_published_posts' => 'edit_pages',
					'delete_others_posts'    => 'edit_pages',
					'edit_private_posts'     => 'edit_pages',
					'edit_published_posts'   => 'edit_pages',
				),
			)
		);
	}

	public function cpt_action_register_taxonomy()
	{

		$category_names = apply_filters( 'cpt_events_category_name',
			array(
				'singular' =>esc_html__( 'Category', 'cpt-events' ),
				'plural'   =>esc_html__( 'Categories', 'cpt-events' )
			) );

		$labels = array(
			'name'              => sprintf( _x( 'Services %s', 'taxonomy general name', 'cpt-events' ),
				$category_names['plural'] ),
			'singular_name'     => sprintf( _x( 'Services %s', 'taxonomy singular name', 'cpt-events' ),
				$category_names['singular'] ),
			'search_items'      => sprintf(esc_html__( 'Search %s', 'cpt-events' ), $category_names['plural'] ),
			'all_items'         => sprintf(esc_html__( 'All %s', 'cpt-events' ), $category_names['plural'] ),
			'parent_item'       => sprintf(esc_html__( 'Parent %s', 'cpt-events' ), $category_names['singular'] ),
			'parent_item_colon' => sprintf(esc_html__( 'Parent %s:', 'cpt-events' ), $category_names['singular'] ),
			'edit_item'         => sprintf(esc_html__( 'Edit %s', 'cpt-events' ), $category_names['singular'] ),
			'update_item'       => sprintf(esc_html__( 'Update %s', 'cpt-events' ), $category_names['singular'] ),
			'add_new_item'      => sprintf(esc_html__( 'Add New %s', 'cpt-events' ), $category_names['singular'] ),
			'new_item_name'     => sprintf(esc_html__( 'New %s Name', 'cpt-events' ), $category_names['singular'] ),
			'menu_name'         => sprintf(esc_html__( '%s', 'cpt-events' ), $category_names['plural'] )
		);
		$args   = array(
			'labels'            => $labels,
			'public'            => true,
			'hierarchical'      => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => false,
			'rewrite'           => array(
				'slug' => $this->taxonomy_slug
			),
		);

		register_taxonomy( self::TAXONOMY_NAME, esc_attr( self::POST_TYPE ), $args );
	}

	private function add_admin_actions()
	{
		require_once (CPT_PLUGIN_PATH.'/includes/Kama_Post_Meta_Box.php');
		require_once (CPT_PLUGIN_PATH.'/includes/CPT_Event_Meta_Data.php');
		$this->create_metabox();

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );
	}
	private function add_admin_filters()
	{

	}

	private function add_client_actions()
	{
		require_once (CPT_PLUGIN_PATH.'/includes/CPT_Event_Meta_Data.php');
		include_once (CPT_PLUGIN_PATH.'/includes/CPT_Query.php');
		include_once (CPT_PLUGIN_PATH.'/shortcodes/cpt_event_list.php');

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_client_scripts' ), 99);
	}
	private function add_client_filters()
	{
		add_filter('single_template', array($this, 'cpt_event_template'));
	}

	public function enqueue_admin_scripts()
	{
		wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js');
		wp_enqueue_script( 'jquery' );

		wp_enqueue_script('jquery-ui-datepicker');
		wp_register_style('jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css');
		wp_enqueue_style('jquery-ui');

		wp_enqueue_script( 'cpt_admin_script', plugin_dir_url( __FILE__ ) . '/js/admin.js', array('jquery'), CPT_PLUGIN_VERSION );


		wp_register_style('cpt-admin-css', plugin_dir_url( __FILE__ ) . '/css/admin.css');
		wp_enqueue_style('cpt-admin-css');
	}

	public function enqueue_client_scripts()
	{
		wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js');
		wp_enqueue_script( 'jquery' );

		wp_register_script( 'cpt_client_script', plugin_dir_url( __FILE__ ) . 'js/client.js', array('jquery'), CPT_PLUGIN_VERSION );
		wp_enqueue_script( 'cpt_client_script' );
		wp_localize_script( 'cpt_client_script', 'headJS', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'templateurl' => get_template_directory_uri(),
			'cptPostType' => self::POST_TYPE,
		));

		wp_register_style('cpt-client-css', plugin_dir_url( __FILE__ ) . '/css/client.css');
		wp_enqueue_style('cpt-client-css');
	}

	private function create_metabox()
	{
		new Kama_Post_Meta_Box( array(
			'id'     => 'events_metabox',
			'title'  => esc_html__( 'Event Params', 'cpt-events' ),
			'post_type'  => self::POST_TYPE,
			'fields' => array(
				'date_from' => array(
					'type' => 'text',
					'title' => esc_html__( 'Date Start', 'cpt-events' ),
					'desc' => esc_html__( '', 'cpt-events' ),
					'attr' => ''
				),
				'date_to' => array(
					'type' => 'text',
					'title' => esc_html__( 'Date End', 'cpt-events' ),
					'desc' => esc_html__( '', 'cpt-events' ),
					'attr' => ''
				),
				'location_lat' => array(
					'type' => 'hidden',
					'attr' => ''
				),
				'location_lng' => array(
					'type' => 'hidden',
					'attr' => ''
				),
				'date_start' => array(
					'type' => 'hidden',
					'attr' => ''
				),
				'date_end' => array(
					'type' => 'hidden',
					'attr' => ''
				),
				'location_name' => array(
					'type' => 'text',
					'callback' => function($args, $post, $name, $val){
						return $this->render_map_field($args, $post, $name, $val);
					},
					'sanitize_func' => 'sanitize_text_field',
					'title' => esc_html__( 'Event Location', 'cpt-events' ),
					'desc' => esc_html__( '', 'cpt-events' ),
					'attr' => ''
				),
			),
		) );
	}

	private function render_map_field($args, $post, $name, $val)
	{
		return '
			<div id="locationField">
			  <input id="autocomplete" placeholder="Enter event address" name="' . esc_attr($name) . '" value="' . esc_attr($val) . '" type="text">
			</div>		
			<div id="map"></div>
			<script src="https://maps.googleapis.com/maps/api/js?key='.self::GOOGLE_MAPS_API_KEY.'&libraries=places&callback=initGooleMapApi" defer></script>	
		';
	}

	public function cpt_event_template($single) {

		global $wp_query, $post;

		/* Checks for single template by post type */
		if ( $post->post_type == self::POST_TYPE) {
			if ( file_exists( __DIR__ . '/templates/single-event.php' ) ) {
				return __DIR__ . '/templates/single-event.php';
			}
		}

		return $single;

	}

	public static function uninstall()
	{
		if ( ! current_user_can( 'activate_plugins' ) )
			exit('You do not have access for this action');

		global $wpdb;
		$wpdb->delete( 'wp_posts', array('post_type' => self::POST_TYPE) );
	}
}

include_once (CPT_PLUGIN_PATH.'/includes/ajax-load-more.php');
register_uninstall_hook( __FILE__,  array('CPT_Events', 'uninstall') );

