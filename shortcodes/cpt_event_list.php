<?php
if(!function_exists('cpt_event_list')) {
	function cpt_event_list($atts) {
		$atts = shortcode_atts( array(
			'category' => '',
			'posts_per_page' => 2,
			'only_future' => false,
		), $atts, 'cpt_event_list' );

		$query = new CPT_Query($atts);

		if($query->have_posts()) : ?>
			<div class="cpt-event-list">

				<?php while ($query->have_posts()) {
				$query->the_post();

				$item_meta = new CPT_Event_Meta_Data(get_the_ID());
				$date_format = get_option( 'date_format' );

				include(CPT_PLUGIN_PATH.'/templates/list-item.php');
			} ?>

			</div>
			<button class="btn load_more"
				data-nonce="<?php echo wp_create_nonce('load_posts') ?>"
				data-only-future="<?php echo esc_attr($atts['only_future']) ?>"
				data-category="<?php echo esc_attr($atts['category']) ?>"
				data-posts_per_page="2">
				   <?php esc_html_e('Load More', 'cpt-events') ?>
			</button>
		<?php endif;
	}
}
add_shortcode('cpt_event_list', 'cpt_event_list');
