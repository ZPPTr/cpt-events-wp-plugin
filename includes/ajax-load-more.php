<?php
require_once (__DIR__.'/CPT_Query.php');

add_action( "wp_ajax_load_more", "get_more_posts" ); // when logged in
add_action( "wp_ajax_nopriv_load_more", "get_more_posts" );//when logged out
//function return new posts based on offset and posts per page value
function get_more_posts() {
	//verifying nonce here
	if ( !wp_verify_nonce( $_REQUEST['nonce'], "load_posts" ) ) {
		exit("No naughty business please");
	}
	$offset = isset($_REQUEST['offset']) ? intval($_REQUEST['offset']) : 0;
	$only_future = isset($_REQUEST['only_future']) ? intval($_REQUEST['only_future']) : 0;
	$posts_per_page = isset($_REQUEST['posts_per_page']) ? intval($_REQUEST['posts_per_page']) : 2;
	//optional, if post type is not defined use regular post type
	$post_type = isset($_REQUEST['post_type'])?$_REQUEST['post_type']:CPT_Events::POST_TYPE;


	ob_start(); // buffer output instead of echoing it
	$args = array(
		'post_type'=>$post_type,
		'offset' => $offset,
		'posts_per_page' => $posts_per_page,
		'only_future' => $only_future,
	);
	$posts_query = new CPT_Query( $args );


	if ($posts_query->have_posts()) {
		//if we have posts:
		$result['have_posts'] = true; //set result array item "have_posts" to true

		while ( $posts_query->have_posts() ) : $posts_query->the_post();
			$item_meta = new CPT_Event_Meta_Data(get_the_ID());
			$date_format = get_option( 'date_format' );

			require(__DIR__.'/../templates/list-item.php');
		endwhile;
		$result['html'] = ob_get_clean(); // put alloutput data into "html" item
	} else {
		//no posts found
		$result['have_posts'] = false; // return that there is no posts found
	}
	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result); // encode result array into json feed
		echo $result; // by echo we return JSON feed on POST request sent via AJAX
	}
	else {
		header("Location: ".$_SERVER["HTTP_REFERER"]);
	}
	die();
}