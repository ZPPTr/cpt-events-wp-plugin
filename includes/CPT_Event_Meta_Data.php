<?php
/**
 * Created by PhpStorm.
 * User: WP-Andrey
 * Date: 17.03.2018
 * Time: 18:37
 */

class CPT_Event_Meta_Data
{

	private $post_id;

	public $date_start;
	public $date_end;
	public $location_name;
	public $location_lat;
	public $location_lng;

	public function __construct($post_id)
	{
		$this->post_id = $post_id;
		$this->init();
	}

	private function init() {
		$this->date_start = get_post_meta( $this->post_id, 'events_metabox_date_start', 1 );
		$this->date_end = get_post_meta( $this->post_id, 'events_metabox_date_end', 1 );
		$this->location_name = get_post_meta( $this->post_id, 'events_metabox_location_name', 1 );
		$this->location_lat = get_post_meta( $this->post_id, 'events_metabox_location_lat', 1 );
		$this->location_lng = get_post_meta( $this->post_id, 'events_metabox_location_lng', 1 );
	}
}