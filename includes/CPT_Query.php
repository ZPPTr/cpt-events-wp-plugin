<?php

class CPT_Query extends WP_Query
{
	public function __construct($args = array())
	{
		$query = array(
			'post_type' => CPT_Events::POST_TYPE,
			'posts_per_page' => isset($args['posts_per_page']) ? (int) $args['posts_per_page'] : 2,
			'offset' => isset($args['offset']) ? (int) $args['offset'] : 0,
			'orderby' => 'meta_value',
			'order' => 'ASC',
			'meta_key' => 'events_metabox_date_start'

		);
		if(isset($args['only_future']) && $args['only_future']) {
			$query['meta_query'] = array(
				array(
					'key' => 'events_metabox_date_start',
					'value' =>  date('Y-m-d', time()),
					'compare' => '>=',
					'type' => 'date'
				),
			);
		}

		if(isset($args['category']) && $args['category']) {
			$query['tax_query'] = array(
				array(
					'taxonomy' => CPT_Events::TAXONOMY_NAME,
					'field'    => 'name',
					'terms'    => $args['category'],
				)
			);
		}

		parent::__construct($query);
	}
}